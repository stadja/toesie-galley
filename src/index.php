<?php
include 'gallery-crawler.php';
/**
 * On a désormais un tableau $galleries qui contient une liste de galeries
 * avec les images associés
 */
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="user-scalable=no, width=device-width, initial-scale=1, maximum-scale=1">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/js/lightbox-plus-jquery.js" integrity="sha512-0rYcJjaqTGk43zviBim8AEjb8cjUKxwxCqo28py38JFKKBd35yPfNWmwoBLTYORC9j/COqldDc9/d1B7dhRYmg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.css" integrity="sha512-Woz+DqWYJ51bpVk5Fv0yES/edIMXjj3Ynda+KWTIkGoynAMHrqTcDUQltbipuiaD5ymEo9520lyoVOo9jCQOCA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
  <title>Galerie d'images</title>

  <style type="text/css" media="screen">
    body {
      font-family: Courier New, Courier, mono;
      font-size: 14px;
      width: calc(100% - 2em);
      max-width: 1025px;
      margin: auto;
      padding: 3em 1em 1em 1em;
    }
    h1 {
      color: #FFA500;
    }
    h2 {
      font-size: 14px;
      text-decoration: underline;
      font-weight: bold;
      margin-bottom: 1em;
    }
    .backlink {
      color: #B40404;
      text-decoration: none;
      margin: 2em 0;
      display: block;
    }
    .gallery {
      padding-bottom: 2em;
    }
    .gallery--title {
      color: #2C3539;
    }
    .gallery--images {
      display: flex;
      flex-wrap: wrap;
      gap: 1em;
      max-height: 80vh;
      overflow: auto;
    }
    .gallery--image {
      height: 300px;
      position: relative;
      overflow: hidden;
    }
    .gallery--image--picture {
      width: auto;
      height: 100%;
      object-fit: cover;
      filter: blur(20px);
      transition: filter 500ms, transform 500ms;
    }
    .gallery--image--picture.gallery--image--picture--loaded {
      filter: blur(0px);
    }

    .gallery--image--title {
      position: absolute;
      width: calc(100% - 1em);
      bottom: 0;
      padding: 0.5em;
      background-color: rgba(0, 0, 0, 0.6);
      color: #fff;
      text-align: center;
      margin: 0;
      margin-bottom: -5em;
      transition: margin 250ms;
    }

    .gallery--image:hover .gallery--image--picture {
      transform: scale(1.05);
    }

    .gallery--image:hover .gallery--image--title {
      margin-bottom: 0;
    }

    aside nav {
      position: fixed;
      right: 0;
      z-index: 10;
      background: rgba(255, 255, 255, 0.8);
      border-top-left-radius: 5px;
      border-bottom-left-radius: 5px;
      padding-left: 0.5em;
    }
  </style>
</head>
<body>
  <header>
    <a class="backlink" href="https://www.toesie.fr" target="_top">
      Retour TOESIE
    </a>
    <h1>Galerie d'images</h1>
  </header>
  <aside>
    <nav>
      <ul dir="rtl">
        <?php foreach($galleries as $gallery): ?>
          <li><a href="#<?php echo $gallery['label']; ?>" class="gallery--title">
            <?php echo $gallery['label']; ?>
            </a></li>
        <?php endforeach; ?>
      </ul>
    </nav>
  </aside>
  <div class="gallery--wrapper">
    <?php foreach($galleries as $gallery): ?>
      <div class="gallery" id="<?php echo $gallery['label']; ?>">
        <a href="#<?php echo $gallery['label']; ?>"><h2 class="gallery--title"><?php echo $gallery['label']; ?></h2></a>
        <div class="gallery--images">
          <?php foreach($gallery['images'] as $image): ?>
            <div class="gallery--image">
              <a href="<?php echo $gallery['path'].'/'.$image['path']; ?>" data-lightbox="<?php echo $gallery['label']; ?>" data-title="<?php echo $image['label']; ?>">
                <img data-lightbox="<?php echo $image['label']; ?>" loading="lazy" class="gallery--image--picture" src="<?php echo $gallery['path'].'/'.$image['path']; ?>"/>
              </a>
              <h3 class="gallery--image--title"><?php echo $image['label']; ?></h3>
            </div>
          <?php endforeach; ?>
        </div>
      </div>
    <?php endforeach; ?>
  </div class="gallery--wrapper">
  <footer>
    <a class="backlink" href="https://www.toesie.fr" target="_top">
      Retour TOESIE
    </a>
  </footer>
  <script>
    let images = document.getElementsByClassName('gallery--image--picture');
    for (let idx in images) {
      let image = images[idx];
      if (!image.addEventListener) {
        continue;
      }
      image.addEventListener('load', function() {
        this.classList.add('gallery--image--picture--loaded');
      })
    };
    setTimeout(() => {
      for (let idx in images) {
        let image = images[idx];
        if (!image.addEventListener) {
          continue;
        }
        image.classList.add('gallery--image--picture--loaded');
      };
    })
  </script>
</body>
</html>
