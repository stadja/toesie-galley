<?php
/**
 *  C'est la partie PHP qui va s'occuper de préparer toutes les variables
*/

// On scane les sous-fichiers
$files = scandir(__DIR__. '/galleries/');
//galleries/ On vérifie que les fichiers sont des dossiers
$folders = array_filter($files, function($file) {
  return is_dir('galleries/' . $file);
});
// On vire les dossiers '.' et '..' qui sont des dossiers systèmes
$folders = array_values(array_filter($folders, function($dirName) {
  return strpos($dirName, '.') !== 0;
}));

// maintenant on crée le tableau des galleries: une gallerie par dossier
$galleries = array_reduce($folders, function($galleries, $item) {
  $label = explode('.', $item);
  array_shift($label);
  $label = implode('.', $label);

  $galleries[] = [
    'path' => 'galleries/' . $item,
    'label' => $label,
    'images' => [],
  ];

  return $galleries;
},[]);

// on boucle sur les galleries pour les remplir avec les images
$galleries = array_map(function($gallery) {
  // on regarde dans le dossier de la gallery
  $files = scandir(__DIR__ . '/' . $gallery['path']);
  // on ne conserve que les fichiers
  $files = array_filter($files, function($file) use($gallery) {
    return is_file(__DIR__ . '/' . $gallery['path'] . '/' . $file);
  });

  // on traite ces fichiers pour les ajouter à la gallery
  $gallery['images'] = array_reduce($files, function($images, $item) {
    $label = explode('.', $item);
    array_pop($label);
    array_shift($label);
    $label = implode('.', $label);

    $images[] = [
      'path' => $item,
      'label' => $label,
    ];

    return $images;
  }, []);

  return $gallery;
}, $galleries);

/**
 * On a désormais un tableau $galleries qui contient une liste de galeries
 * avec les images associés
 */
